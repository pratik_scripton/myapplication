package com.example.hp.myapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by HP on 21-08-2017.
 */

public class AccountModel {

    @SerializedName("list")

    private ArrayList<ListModel> list = null;

    @SerializedName("code")
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<ListModel> getList() {
        return list;
    }

}
