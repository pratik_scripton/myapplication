//package com.example.hp.myapplication.Fragments;
//
//
//import android.app.Dialog;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.example.hp.myapplication.R;
//import com.example.hp.myapplication.rest.ResponseListener;
//
//import retrofit2.Response;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class BlankFragment extends Fragment implements ResponseListener{
//
//
//    public BlankFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_blank, container, false);
//    }
//
//    @Override
//    public void onResponseSuccess(Response baseResponse, Dialog dialog) {
//
//    }
//
//    @Override
//    public void onResponseFailure(Throwable throwable, Dialog dialog) {
//
//    }
//
//    @Override
//    public void onMessageRecieved(String msg) {
//
//    }
//}
