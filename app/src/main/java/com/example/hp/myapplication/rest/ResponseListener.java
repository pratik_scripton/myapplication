package com.example.hp.myapplication.rest;

import android.app.Dialog;


import retrofit2.Response;

/**
 * Created by aslesha.more on 4/21/2017.
 */

public interface ResponseListener {
  //  void onResponseSuccess(Response baseResponse, Dialog dialog);
    void onResponseSuccess(Response baseResponse);
 //  void onResponseFailure(Throwable throwable, Dialog dialog);
    void onResponseFailure(Throwable throwable);
    void onMessageRecieved(String msg);

}
