package com.example.hp.myapplication.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.myapplication.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        BindId();
    }


    public void BindId(){

        ImageView imghome= (ImageView) findViewById(R.id.imghome);
        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        ImageView imgnotification= (ImageView) findViewById(R.id.imgnotification);
        imgnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProfileActivity.this,NotificationActivity.class);
                startActivity(intent);
                finish();
            }
        });

        TextView txtprofile=(TextView)findViewById(R.id.txtprofile);
        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");
        txtprofile.setTypeface(type);
    }
}
