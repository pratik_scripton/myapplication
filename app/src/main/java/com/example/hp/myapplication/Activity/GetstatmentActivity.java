package com.example.hp.myapplication.Activity;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.example.hp.myapplication.Adapters.MySpinnersAdapter;
import com.example.hp.myapplication.R;

import java.util.Calendar;

public class GetstatmentActivity extends AppCompatActivity implements
        com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener,AdapterView.OnItemSelectedListener {
    RadioButton rdlmonth,rd3months,rd6months,rd1year;
    TextView txt10,txt20,txt30,txt40,txtdateto,txtdatefrom;
    String a="",b="",c="",d="";
    LinearLayout lnr_date;
    private boolean mAutoHighlight;
    Spinner spinerorderby;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getstatment);
        BindId();
        onclick ();




    }

    public void BindId(){
       TextView txtgetstatments=(TextView)findViewById(R.id.txtgetstatments);
       TextView txtstatements=(TextView)findViewById(R.id.txtstatements);
       TextView txtduration=(TextView)findViewById(R.id.txtduration);
       TextView txttransaction=(TextView)findViewById(R.id.txttransaction);
       TextView txtselectpages=(TextView)findViewById(R.id.txtperpages);
       TextView textView=(TextView)findViewById(R.id.textView);
       TextView txtorderby=(TextView)findViewById(R.id.txtorderby);
        txt10=(TextView)findViewById(R.id.txt10);
        txt20=(TextView)findViewById(R.id.txt20);
        txt30=(TextView)findViewById(R.id.txt30);
        txt40=(TextView)findViewById(R.id.txt40);
        txtdateto=(TextView)findViewById(R.id.txtdateto);
        txtdatefrom=(TextView)findViewById(R.id.txtdatefrom);
        rdlmonth=(RadioButton) findViewById(R.id.rdlmonth);
        rd3months=(RadioButton) findViewById(R.id.rd3months);
        rd6months=(RadioButton) findViewById(R.id.rd6months);
        rd1year=(RadioButton) findViewById(R.id.rd1year);
        lnr_date=(LinearLayout) findViewById(R.id.lnr_date);
        spinerorderby=(Spinner) findViewById(R.id.spinerorderby);

      Spinner  spinner = (Spinner) findViewById(R.id.spinner);
        spinerorderby.setOnItemSelectedListener(GetstatmentActivity.this);

//        String[] Accountnumbers={"501003256623","501003256624","501003256625"};
//
//        MySpinnersAdapter adapter = new MySpinnersAdapter(GetstatmentActivity.this,Accountnumbers);
//        // adapter.setDropDownViewResource(R.layout.addshopcat);
//        spinner.setAdapter(adapter);

        String[] country = { "Ascending","Descending",  };
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,R.layout.order_list,R.id.txtordertext,country);
        aa.setDropDownViewResource(R.layout.order_list);
        //Setting the ArrayAdapter data on the Spinner
        spinerorderby.setAdapter(aa);

        Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtgetstatments.setTypeface(type);
        txtstatements.setTypeface(type);
        txtduration.setTypeface(type);
        txttransaction.setTypeface(type);
        txtselectpages.setTypeface(type);
        textView.setTypeface(type);
        rd1year.setTypeface(type);
        rdlmonth.setTypeface(type);
        rd3months.setTypeface(type);
        rd6months.setTypeface(type);
        txtorderby.setTypeface(type);
        txt10.setTypeface(type1);
        txtdateto.setTypeface(type1);
        txtdatefrom.setTypeface(type1);
    }

    public void onclick (){



        lnr_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        GetstatmentActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });


        rdlmonth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rd3months.setChecked(false);
                    rd6months.setChecked(false);
                    rd1year.setChecked(false);
                    if(txtdatefrom.length()>0) {
                        txtdatefrom.setText("");
                        txtdateto.setText("");
                        txtdatefrom.setHint("DD/MM/YYYY");
                        txtdateto.setHint("DD/MM/YYYY");
                    }
                }
                else {
                    rdlmonth.setChecked(false);
                }
            }
        });


        rd3months.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rdlmonth.setChecked(false);
                    rd6months.setChecked(false);
                    rd1year.setChecked(false);
                    if(txtdatefrom.length()>0) {
                        txtdatefrom.setText("");
                        txtdateto.setText("");
                        txtdatefrom.setHint("DD/MM/YYYY");
                        txtdateto.setHint("DD/MM/YYYY");
                    }
                }
                else {
                    rd3months.setChecked(false);
                }
            }
        });

        rd6months.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rdlmonth.setChecked(false);
                    rd3months.setChecked(false);
                    rd1year.setChecked(false);
                    if(txtdatefrom.length()>0) {
                        txtdatefrom.setText("");
                        txtdateto.setText("");
                        txtdatefrom.setHint("DD/MM/YYYY");
                        txtdateto.setHint("DD/MM/YYYY");
                    }
                }
                else {
                    rd6months.setChecked(false);
                }
            }
        });

        rd1year.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rdlmonth.setChecked(false);
                    rd3months.setChecked(false);
                    rd6months.setChecked(false);
                    if(txtdatefrom.length()>0) {
                        txtdatefrom.setText("");
                        txtdateto.setText("");
                        txtdatefrom.setHint("DD/MM/YYYY");
                        txtdateto.setHint("DD/MM/YYYY");
                    }
                }
                else {
                    rd1year.setChecked(false);
                }
            }
        });


        txt10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(a.equals("")) {
                    a="selected";
                    b="";
                    c="";
                    d="";
                    txt10.setBackgroundResource(R.drawable.colorroundedbackground);
                    txt20.setBackgroundResource(R.drawable.roundedbackground);
                    txt30.setBackgroundResource(R.drawable.roundedbackground);
                    txt40.setBackgroundResource(R.drawable.roundedbackground);
                }
                else {
                    a="";
                    txt10.setBackgroundResource(R.drawable.roundedbackground);
                }
            }
        });

        txt20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b.equals("")) {
                    b="selected";
                    a="";
                    d="";
                    c="";
                    txt10.setBackgroundResource(R.drawable.roundedbackground);
                    txt20.setBackgroundResource(R.drawable.colorroundedbackground);
                    txt30.setBackgroundResource(R.drawable.roundedbackground);
                    txt40.setBackgroundResource(R.drawable.roundedbackground);
                }
                else {
                    b="";
                    txt20.setBackgroundResource(R.drawable.roundedbackground);
                }
            }
        });

        txt30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(c.equals("")) {
                    c="selected";
                    a="";
                    b="";
                    d="";
                    txt10.setBackgroundResource(R.drawable.roundedbackground);
                    txt20.setBackgroundResource(R.drawable.roundedbackground);
                    txt30.setBackgroundResource(R.drawable.colorroundedbackground);
                    txt40.setBackgroundResource(R.drawable.roundedbackground);
                }
                else {
                    c="";
                    txt30.setBackgroundResource(R.drawable.roundedbackground);
                }
            }
        });

        txt40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(d.equals("")) {
                    d="selected";
                    a="";
                    b="";
                    c="";
                    txt10.setBackgroundResource(R.drawable.roundedbackground);
                    txt20.setBackgroundResource(R.drawable.roundedbackground);
                    txt30.setBackgroundResource(R.drawable.roundedbackground);
                    txt40.setBackgroundResource(R.drawable.colorroundedbackground);
                }
                else {
                    d="";
                    txt40.setBackgroundResource(R.drawable.roundedbackground);
                }
            }
        });


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {

    }

    @Override
    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String datefrom = dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        String dateto = dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        rdlmonth.setChecked(false);
        rd3months.setChecked(false);
        rd6months.setChecked(false);
        rd1year.setChecked(false);
        txtdateto.setText(dateto);
        txtdatefrom.setText(datefrom);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
