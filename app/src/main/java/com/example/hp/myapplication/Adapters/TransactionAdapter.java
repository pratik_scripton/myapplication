package com.example.hp.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.hp.myapplication.R;
import com.example.hp.myapplication.models.ListModel;

import java.util.ArrayList;



/**
 * Created by androapps7 on 26/9/16.
 */
public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    Context context;
    ArrayList<ListModel> transactionlist;
    String type;

    public TransactionAdapter(Context context, ArrayList<ListModel> transactionlist,String type) {

        this.transactionlist = transactionlist;
        this.context = context;
        this.type = type;
    }


    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(v, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



      if(type.equals("all")) {
          holder.txtnaration.setText(transactionlist.get(position).getTransaction_narration());
          holder.txtamount.setText(transactionlist.get(position).getAmount());
          if (transactionlist.get(position).getTransaction_type().equals("Credit")) {
              holder.imgtransaction.setImageResource(R.drawable.rupeecredit);
          } else {
              holder.imgtransaction.setImageResource(R.drawable.rupeedebit);
          }
      }

           else if(type.equals("Debit")) {

         if (transactionlist.get(position).getTransaction_type().equals("Debit")) {
              holder.txtnaration.setText(transactionlist.get(position).getTransaction_narration());
              holder.txtamount.setText(transactionlist.get(position).getAmount());
              holder.imgtransaction.setImageResource(R.drawable.rupeedebit);

          } else {
              holder.imgline.setVisibility(View.GONE);
              holder.txtnaration.setVisibility(View.GONE);
              holder.txtamount.setVisibility(View.GONE);
              holder.imgtransaction.setVisibility(View.GONE);
          }
      }


    }



    @Override
    public int getItemCount() {
        return transactionlist.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtnaration, txtamount, txtStatus,txtOrder_id;
        RelativeLayout lnr;
        ImageView imgtransaction;
        ImageView imgline;

        public ViewHolder(View itemView, int viewtype) {
            super(itemView);
            imgtransaction = (ImageView) itemView.findViewById(R.id.imgtransaction);
            imgline = (ImageView) itemView.findViewById(R.id.imgline);
            txtnaration = (TextView) itemView.findViewById(R.id.txtnaration);
            txtamount = (TextView) itemView.findViewById(R.id.txtamount);



        }

    }

}
