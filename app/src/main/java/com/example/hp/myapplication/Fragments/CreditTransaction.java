package com.example.hp.myapplication.Fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.hp.myapplication.Activity.GetstatmentActivity;
import com.example.hp.myapplication.Adapters.TransactionAdapter;
import com.example.hp.myapplication.Adapters.TransactionCreditAdapter;
import com.example.hp.myapplication.Interfaces.PageRefreshListener;
import com.example.hp.myapplication.R;
import com.example.hp.myapplication.models.ListModel;

import java.util.ArrayList;

/**
 * Created by HP on 19-07-2017.
 */

public class CreditTransaction  extends Fragment implements PageRefreshListener {

    ArrayList<ListModel> transactionlist;
    RecyclerView rcycreditlist;
    private TransactionCreditAdapter adapter;

    public CreditTransaction() {
//        transactionlist=transactionmodel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.credit_transaction, container, false);

        RelativeLayout rlt_lastdate= (RelativeLayout) rootView.findViewById(R.id.rlt_lastdate);
        rcycreditlist= (RecyclerView) rootView.findViewById(R.id.rcycreditlist);
        TextView txtlasttransactiondate= (TextView) rootView.findViewById(R.id.txtlasttransactiondate);

        Typeface type = Typeface.createFromAsset(getContext().getAssets(),
                "DaxlinePro-Bold_13127.ttf");
        txtlasttransactiondate.setTypeface(type);


        rlt_lastdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(), GetstatmentActivity.class);
                startActivity(intent);
            }
        });

//        TransactionCreditAdapter adapter=new TransactionCreditAdapter(getContext(),transactionlist,"Credit");
//        LinearLayoutManager rec_layout_manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        rcycreditlist.setLayoutManager(rec_layout_manager);
//        rcycreditlist.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onPageRefresh(ArrayList<ListModel> transactionmodel) {
        transactionlist=transactionmodel;
        adapter = new TransactionCreditAdapter(getContext(), transactionlist, "Credit");
        LinearLayoutManager rec_layout_manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcycreditlist.setLayoutManager(rec_layout_manager);
        rcycreditlist.setAdapter(adapter);
    }
}
