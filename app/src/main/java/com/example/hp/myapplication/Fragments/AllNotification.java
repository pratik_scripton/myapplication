package com.example.hp.myapplication.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import com.example.hp.myapplication.R;

/**
 * Created by HP on 25-07-2017.
 */

public class AllNotification extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.allnotification, container, false);

        return rootView;
    }
}
