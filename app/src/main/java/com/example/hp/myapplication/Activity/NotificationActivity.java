package com.example.hp.myapplication.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.myapplication.Adapters.ViewPagerAdapter;
import com.example.hp.myapplication.Fragments.Alerts;
import com.example.hp.myapplication.Fragments.AllNotification;
import com.example.hp.myapplication.Fragments.AllTransaction;
import com.example.hp.myapplication.Fragments.CreditTransaction;
import com.example.hp.myapplication.Fragments.DebitTransaction;
import com.example.hp.myapplication.Fragments.Offers;
import com.example.hp.myapplication.R;

public class NotificationActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    Context context = this;
//    private int[] tabIcons = {
//            R.mipmap.tab1,
//            R.mipmap.tab3,
//            R.mipmap.tab2
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initialise();
      //  tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffff"));


        tabLayout.setTabTextColors(
                ContextCompat.getColor(context, R.color.first),
                ContextCompat.getColor(context, R.color.first)
        );



    }

    private void initialise() {


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
        TextView txtnotification=(TextView)findViewById(R.id.txtnotification);
        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");
        txtnotification.setTypeface(type);


        ImageView imghome= (ImageView) findViewById(R.id.imghome);
        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NotificationActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        ImageView imgprofile= (ImageView) findViewById(R.id.imgprofile);
        imgprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NotificationActivity.this,ProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });
       // setupTabIcons();


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllNotification(), "All");
        adapter.addFragment(new Alerts(), "Alerts");
        adapter.addFragment(new Offers(), "Offers");
        viewPager.setAdapter(adapter);
    }

//    private void setupTabIcons() {
//        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
//        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
//        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
//    }

}
