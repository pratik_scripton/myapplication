package com.example.hp.myapplication.Activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.hp.myapplication.Adapters.ViewPagerAdapter;
import com.example.hp.myapplication.Fragments.AllTransaction;
import com.example.hp.myapplication.Fragments.CreditTransaction;
import com.example.hp.myapplication.Fragments.DebitTransaction;
import com.example.hp.myapplication.R;

public class DemoActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);



        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.slidingTabs);


      //  setupViewPager(viewPager);
//        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
    }

//    private void setupViewPager(ViewPager viewPager) {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(new AllTransaction(), "All");
//        adapter.addFragment(new CreditTransaction(), "Credit");
//        adapter.addFragment(new DebitTransaction(), "Debit");
//        viewPager.setAdapter(adapter);
//    }
}
