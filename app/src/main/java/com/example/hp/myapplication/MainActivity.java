package com.example.hp.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.myapplication.Activity.EnrollActivity;
import com.example.hp.myapplication.Activity.ForgotapppinActivity;
import com.example.hp.myapplication.Activity.HomeActivity;
import com.example.hp.myapplication.Activity.SecureActivity;
import com.example.hp.myapplication.Activity.VerifiedActivity;
import com.example.hp.myapplication.Interfaces.Holder;
import com.example.hp.myapplication.models.LoginModel;
import com.example.hp.myapplication.models.OtpModel;
import com.example.hp.myapplication.rest.NetworkController;
import com.example.hp.myapplication.rest.ResponseListener;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnBackPressListener;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.HashMap;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    EditText editText1,editText2,editText3,editText4;
    Typeface type1,type;
    HashMap<String, String> params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BindID();

        RelativeLayout rlt_reg=(RelativeLayout)findViewById(R.id.rlt_reg);
        RelativeLayout rltlogin=(RelativeLayout)findViewById(R.id.rltlogin);

        TextView txtforgotpin= (TextView) findViewById(R.id.txtforgotpin);

        txtforgotpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showChangeLangDialog();
            }
        });

        rlt_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,EnrollActivity.class);
                startActivity(intent);
            }
        });

        rltlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pin=editText1.getText().toString()+editText2.getText().toString()+editText3.getText().toString()+editText4.getText().toString();
                Login(pin);
            }
        });

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText1.getText().toString().length()>0){
                    editText1.clearFocus();
                    editText2.requestFocus();
                    editText2.setCursorVisible(true);
                }
            }
        });



        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText2.getText().toString().length()>0){
                    editText2.clearFocus();
                    editText3.requestFocus();
                    editText3.setCursorVisible(true);
                }

                else {
                    editText2.clearFocus();
                    editText1.requestFocus();
                    editText1.setCursorVisible(true);

                }
            }
        });


        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText3.getText().toString().length()>0){
                    editText3.clearFocus();
                    editText4.requestFocus();
                    editText4.setCursorVisible(true);
                }
                else {
                    editText3.clearFocus();
                    editText2.requestFocus();
                    editText2.setCursorVisible(true);
                }
            }
        });


        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(editText4.getText().toString().length()>0){

                }
                else {
                    editText4.clearFocus();
                    editText3.requestFocus();
                    editText3.setCursorVisible(true);
                }


            }
        });

    }


    public void BindID(){
        editText1=(EditText)findViewById(R.id.editText1);
        editText2=(EditText)findViewById(R.id.editText2);
        editText3=(EditText)findViewById(R.id.editText3);
        editText4=(EditText)findViewById(R.id.editText4);

     TextView txtenter4digit=(TextView) findViewById(R.id.txtenter4digit);
     TextView txtregistered=(TextView) findViewById(R.id.txtregistered);
     TextView txtloginnow=(TextView) findViewById(R.id.txtloginnow);
     TextView txtforgotpin=(TextView) findViewById(R.id.txtforgotpin);
     TextView txtregisternow=(TextView) findViewById(R.id.txtregisternow);
     TextView txtterms=(TextView) findViewById(R.id.txtterms);
     TextView txtcopyright=(TextView) findViewById(R.id.txtcopyright);



         type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

       type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtenter4digit.setTypeface(type1);
        txtregistered.setTypeface(type1);
        txtloginnow.setTypeface(type);
        txtforgotpin.setTypeface(type);
        txtregisternow.setTypeface(type);
        txtterms.setTypeface(type);
        txtcopyright.setTypeface(type);
    }

    public void showChangeLangDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater =MainActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edtmobilenumber);

        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Enter Registred Mobile Number");
        //dialogBuilder.setMessage("Verification code sent to your mobile number ");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        final AlertDialog b = dialogBuilder.create();
        b.show();
        Button pos = b.getButton(DialogInterface.BUTTON_POSITIVE);
        pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MainActivity.this,ForgotapppinActivity.class);
                startActivity(intent);
                b.dismiss();

            }
        });
    }

    private void Login(final String pin) {

        String android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String imeinumber= telephonyManager.getDeviceId();
        String reqString = Build.MANUFACTURER
                + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();


        params=new HashMap<>();
        params.put("app_pin",pin);
        params.put("device_id",android_id);
        params.put("device_imei",imeinumber);
        NetworkController.getInstance().Login(params, new ResponseListener() {


            @Override
            public void onResponseSuccess(Response baseResponse) {
                String code= String.valueOf((baseResponse.code()));
                if  (code.equals("200")) {
                    LoginModel loginmodel = (LoginModel) baseResponse.body();
                    processResponse(loginmodel,pin);
                }

            }

            @Override
            public void onResponseFailure(Throwable throwable) {
                System.out.print("");
                Toast.makeText(MainActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMessageRecieved(String msg) {

            }
        });
    }


    private void processResponse(LoginModel loginmodel,String pin) {
        // PreferenceHelper.getInstance(getContext()).setStringValue(VariableConstant.AUTH_TOKEN, loginModel.getAccess_token());
        String code = loginmodel.getCode();
        if (code.equals("1")) {
            String userid=loginmodel.getUser_id();
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            SharedPreferences.Editor editor = getSharedPreferences("BOIPASSBOOK", MODE_PRIVATE).edit();
            editor.putString("userid", userid);
            editor.putString("pin", pin);
            editor.apply();
            startActivity(intent);
            finish();
        }
    }




}
