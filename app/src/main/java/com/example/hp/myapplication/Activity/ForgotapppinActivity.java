package com.example.hp.myapplication.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.hp.myapplication.R;

public class ForgotapppinActivity extends AppCompatActivity {
    Button btnsubmit,btnresend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotapppin);


        BindId();



        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ForgotapppinActivity.this,SetNewApppinActivity.class);
                startActivity(intent);
            }
        });
    }

    public void BindId(){
        btnsubmit= (Button) findViewById(R.id.btnsubmit);
        btnresend= (Button) findViewById(R.id.btnresend);
      TextView  txtenter4digit=(TextView)findViewById(R.id.txtenter4digit);




        Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtenter4digit.setTypeface(type1);

        btnsubmit.setTypeface(type);
        btnresend.setTypeface(type);


    }


}
