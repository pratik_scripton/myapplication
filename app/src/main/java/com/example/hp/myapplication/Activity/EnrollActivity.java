package com.example.hp.myapplication.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapplication.R;
import com.example.hp.myapplication.constants.VariableConstant;
import com.example.hp.myapplication.constants.url;
import com.example.hp.myapplication.models.OtpModel;
import com.example.hp.myapplication.models.responeses.LoginResponseModel;
import com.example.hp.myapplication.rest.ApiListener;
import com.example.hp.myapplication.rest.NetworkController;
import com.example.hp.myapplication.rest.ResponseListener;
import com.google.gson.Gson;
import com.squareup.picasso.Downloader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class EnrollActivity extends AppCompatActivity {
    EditText edtmobilenumber;
    ApiListener apiInterface;
    HashMap<String, String> params;
    ImageView imgchck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll);
        BindId();


        LinearLayout lnrsubmit=(LinearLayout)findViewById(R.id.lnrsubmit);
        lnrsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobnumber=edtmobilenumber.getText().toString();

              if  (mobnumber.equals("") || mobnumber.length() > 10 || mobnumber.length() < 10) {
                  edtmobilenumber.requestFocus();
                  edtmobilenumber.setError("Must be 10 digits");
              }else {

                //  Mobileotp(mobnumber);
                  sendMobilenumberRequest(mobnumber);
              }

            }
        });


        edtmobilenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                     if(edtmobilenumber.getText().toString().length()<10){
                         imgchck.setVisibility(View.INVISIBLE);
                     }
                     else {
                         imgchck.setVisibility(View.VISIBLE);
                     }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




    }


    public void BindId(){
        TextView txtenroll=(TextView)findViewById(R.id.txtenroll);
        TextView txtverify=(TextView)findViewById(R.id.txtverify);
        TextView txtsecure=(TextView)findViewById(R.id.txtsecure);
        TextView txtregis=(TextView)findViewById(R.id.txtregis);
        TextView txt91number=(TextView)findViewById(R.id.txt91number);
        edtmobilenumber= (EditText) findViewById(R.id.edtmobilenumber);
        imgchck= (ImageView) findViewById(R.id.imgchck);
        Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtenroll.setTypeface(type);
        txtverify.setTypeface(type);
        txtsecure.setTypeface(type);
        txtregis.setTypeface(type);
        edtmobilenumber.setTypeface(type1);
        txt91number.setTypeface(type1);
    }

    private void sendMobilenumberRequest(final String mobile) {
        params=new HashMap<>();
        params.put("mobile",mobile);
        NetworkController.getInstance().Otp(params, new ResponseListener() {


            @Override
            public void onResponseSuccess(Response baseResponse) {
                String code= String.valueOf((baseResponse.code()));
                if  (code.equals("200")) {
                    OtpModel otpmodel = (OtpModel) baseResponse.body();
                    processResponse(mobile,otpmodel);
                }

            }

            @Override
            public void onResponseFailure(Throwable throwable) {
                System.out.print("");
                Toast.makeText(EnrollActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMessageRecieved(String msg) {

            }
        });
            }

    private void processResponse(String mobilenumber,OtpModel otpModel) {
        // PreferenceHelper.getInstance(getContext()).setStringValue(VariableConstant.AUTH_TOKEN, loginModel.getAccess_token());
        String code = otpModel.getCode();
        if (code.equals("1")) {
            Intent intent = new Intent(EnrollActivity.this, VerifiedActivity.class);
            intent.putExtra("mobnumber", mobilenumber);
            startActivity(intent);
            finish();
        }
    }






}
