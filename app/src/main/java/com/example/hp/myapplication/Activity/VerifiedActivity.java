package com.example.hp.myapplication.Activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapplication.R;
import com.example.hp.myapplication.constants.url;
import com.example.hp.myapplication.models.VerifyotpModel;
import com.example.hp.myapplication.rest.NetworkController;
import com.example.hp.myapplication.rest.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class VerifiedActivity extends AppCompatActivity {
    static EditText edtenterotp;
    TextView txtnumber;
    String mobnumber;
    HashMap<String, String> params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verified);
        Bindid();
        Intent mintent=getIntent();
        mobnumber=mintent.getStringExtra("mobnumber");
        txtnumber.setText("+91 - "+mobnumber);

        LinearLayout lnrnext=(LinearLayout)findViewById(R.id.lnrnext);
        lnrnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendOtp();
               // VerifyotpModel();

            }
        });


    }

    public void Bindid(){
        edtenterotp= (EditText) findViewById(R.id.edtenterotp);
        TextView txtenroll=(TextView)findViewById(R.id.txtenroll);
        TextView txtverify=(TextView)findViewById(R.id.txtverify);
        TextView txtsecure=(TextView)findViewById(R.id.txtsecure);
        txtnumber=(TextView)findViewById(R.id.txtnumber);
        TextView txt4digit=(TextView)findViewById(R.id.txt4digit);
        TextView txtresend=(TextView)findViewById(R.id.txtresend);
        EditText edtenterotp=(EditText) findViewById(R.id.edtenterotp);

        Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtenroll.setTypeface(type);
        txtverify.setTypeface(type);
        txtsecure.setTypeface(type);
        txtresend.setTypeface(type);
        txtnumber.setTypeface(type1);
        txt4digit.setTypeface(type1);
        edtenterotp.setTypeface(type1);

    }

    public static class Broadcast extends BroadcastReceiver {
        IntentFilter filter=new IntentFilter("com.example.hp.myapplication");



// SmsManager sms = SmsManager.getDefault();

        //@Override
        public void onReceive(Context context, Intent intent) {

            // Retrieves a map of extended data from the intent.
            final Bundle bundle = intent.getExtras();

            try {

                if (bundle != null) {

                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (int i = 0; i < pdusObj.length; i++) {

                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                        String senderNum = phoneNumber;
                        String message = currentMessage.getDisplayMessageBody();
                        //String testString = "This is a sentence";
                        String[] parts = message.split(":-");
                        String lastWord = parts[parts.length - 1];
                        //  System.out.println(lastWord); //
                        edtenterotp.setText(lastWord);
                        edtenterotp.performClick();



                        Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);


                        // Show Alert
                        //    int duration = Toast.LENGTH_LONG;
                        //         Toast toast = Toast.makeText(context,
                        //            "senderNum: "+ senderNum + ", message: " + message, duration);
                        //     toast.show();

                    } // end for loop
                } // bundle is null

            } catch (Exception e) {
                Log.e("SmsReceiver", "Exception smsReceiver" +e);

            }
        }
    }


    private void SendOtp() {
        params=new HashMap<>();
        params.put("mobile",mobnumber);
        params.put("otp",edtenterotp.getText().toString().trim());
        NetworkController.getInstance().Verifyotp(params, new ResponseListener() {


            @Override
            public void onResponseSuccess(Response baseResponse) {
                String code= String.valueOf((baseResponse.code()));
                if  (code.equals("200")) {
                    VerifyotpModel verifyotpModel = (VerifyotpModel) baseResponse.body();
                    processResponse(mobnumber, verifyotpModel);
                }

            }

            @Override
            public void onResponseFailure(Throwable throwable) {
                System.out.print("");
                Toast.makeText(VerifiedActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMessageRecieved(String msg) {

            }
        });
    }



    private void processResponse(String mobilenumber,VerifyotpModel verifyotpModel) {
        // PreferenceHelper.getInstance(getContext()).setStringValue(VariableConstant.AUTH_TOKEN, loginModel.getAccess_token());
        String code = verifyotpModel.getCode();
        if (code.equals("1")) {
            String userid= verifyotpModel.getUser_id();
            Intent intent = new Intent(VerifiedActivity.this, SecureActivity.class);
            intent.putExtra("userid",userid );
            startActivity(intent);
            finish();
        }else{

            Toast.makeText(VerifiedActivity.this, "Wrong OTP", Toast.LENGTH_SHORT).show();
        }
    }



}
