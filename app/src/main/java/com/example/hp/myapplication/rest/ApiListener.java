
package com.example.hp.myapplication.rest;

import com.example.hp.myapplication.models.AccountModel;
import com.example.hp.myapplication.models.BaseModel;
import com.example.hp.myapplication.models.LoginModel;
import com.example.hp.myapplication.models.OtpModel;
import com.example.hp.myapplication.models.TransactionModel;
import com.example.hp.myapplication.models.VerifyotpModel;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface ApiListener {
    @FormUrlEncoded
    @POST("sendOtp.php")
   // Call<LoginResponseModel> login(@Field("username") String username, @Field("Password") String password, @Field("grant_type") String grant_type);
    Call<OtpModel> OTP(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("verifyOtp.php")
        // Call<LoginResponseModel> login(@Field("username") String username, @Field("Password") String password, @Field("grant_type") String grant_type);
    Call<VerifyotpModel> verifyotp(@Field("mobile") String mobile, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("setmpin.php")
        // Call<LoginResponseModel> login(@Field("username") String username, @Field("Password") String password, @Field("grant_type") String grant_type);
    Call<BaseModel> Myapppin(@Field("user_id") String userid, @Field("mpin") String mpin,@Field("device_model") String model,@Field("device_imei") String imei,@Field("device_os_version") String android,@Field("device_id") String device_id,@Field("device_manufacture") String make);

    @FormUrlEncoded
    @POST("account_details.php")
        // Call<LoginResponseModel> login(@Field("username") String username, @Field("Password") String password, @Field("grant_type") String grant_type);
    Call<AccountModel> Myaccountnumbers(@Field("user_id") String userid, @Field("mpin") String mpin, @Field("device_model") String model, @Field("device_imei") String imei, @Field("device_os_version") String android, @Field("device_id") String device_id, @Field("device_manufacture") String make);


    @FormUrlEncoded
    @POST("transaction_details.php")
        // Call<LoginResponseModel> login(@Field("username") String username, @Field("Password") String password, @Field("grant_type") String grant_type);
    Call<TransactionModel> MyTransactiondetails(@Field("account_id") String userid);


    @FormUrlEncoded
    @POST("login.php")
        // Call<LoginResponseModel> login(@Field("username") String username, @Field("Password") String password, @Field("grant_type") String grant_type);
    Call<LoginModel> LoginPin(@Field("app_pin") String app_pin, @Field("device_id") String device_id, @Field("device_imei") String device_imei);


//    @GET("api/DiscountCoupon")
//    Call<List<CouponResponseModel>> getDiscountCoupons(@Header("Authorization") String authToken);
//
//    @GET("api/Users")
//    Call<TotalUserResponseModel> getUsers(@Header("Authorization") String authToken, @Query("currentPage") int currentPage, @Query("pageSize") int pageSize);
//
//    @POST("api/Users")
//    Call<BaseResponseModel> updateUser(@Header("Authorization") String authToken, @Body UserRequestModel userRequestModel);
//
//    @PUT("api/Users")
//    Call<BaseResponseModel> insertUser(@Header("Authorization") String authToken, @Body UserRequestModel userRequestModel);
//
//    @DELETE("api/Users/6")
//    Call<BaseResponseModel> deleteUser(@Header("Authorization") String authToken, @Path("UserId") String userID);
//
//    @GET("api/products?currentPage=shdjs")
//    Call<List<ProductResponseModel>> getProducts(@Header("Authorization") String authToken, @Query("currentPage") int currentPage, @Query("pageSize") int pageSize);
//
//    @GET("api/Products")
//    Call<ProductSpinnerResponseModel> getProductSpinnerList(@Header("Authorization") String authToken, @Query("loadAdditionalData") String loadAdditionalData);
//
//    @POST("api/Products")
//    Call<BaseResponseModel> updateProduct(@Header("Authorization") String authToken, @Body ProductResponseModel productResponseModel);
//
//    @PUT("api/Products")
//    Call<BaseResponseModel> insertProduct(@Header("Authorization") String authToken, @Body ProductResponseModel productResponseModel);
//
//    @DELETE("api/Products/{ProductId}")
//    Call<BaseResponseModel> deleteProduct(@Header("Authorization") String authToken, @Path("ProductId") String userID);
//
//    @GET("api/DiscountCoupon/{DiscountId}")
//    Call<BaseResponseModel> deleteDiscountCoupon(@Header("Authorization") String authToken, @Path("DiscountId") String discountId);
}

