package com.example.hp.myapplication.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 20-08-2017.
 */

public class BaseModel {

    @SerializedName("code")
    String code;

    @SerializedName("message")
    String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
