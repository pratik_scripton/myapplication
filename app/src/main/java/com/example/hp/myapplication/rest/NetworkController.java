
package com.example.hp.myapplication.rest;

import android.util.Log;

import com.example.hp.myapplication.models.AccountModel;
import com.example.hp.myapplication.models.BaseModel;
import com.example.hp.myapplication.models.LoginModel;
import com.example.hp.myapplication.models.OtpModel;
import com.example.hp.myapplication.models.TransactionModel;
import com.example.hp.myapplication.models.VerifyotpModel;


import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class NetworkController {

    private static String BASE_URL = "http://13.126.236.163/BOI/webservice/";
    private ApiListener apiService;

    public  NetworkController() {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(ApiListener.class);

    }

    public static NetworkController getInstance() {
        return new NetworkController();
    }

    public void Otp(HashMap<String, String> params, ResponseListener responseListener) {

        Call<OtpModel> call = apiService.OTP(params.get("mobile"));
        serverCall(call, responseListener);
    }


    public void Login(HashMap<String, String> params, ResponseListener responseListener) {

        Call<LoginModel> call = apiService.LoginPin(params.get("app_pin"),params.get("device_id"),params.get("device_imei"));
        serverCall(call, responseListener);
    }


    public void Verifyotp(HashMap<String, String> params, ResponseListener responseListener) {

        Call<VerifyotpModel> call = apiService.verifyotp(params.get("mobile"),
                params.get("otp"));
        serverCall(call, responseListener);
    }



    public void Setapppin(HashMap<String, String> params, ResponseListener responseListener) {

        Call<BaseModel> call = apiService.Myapppin(params.get("user_id"),
                params.get("mpin"),params.get("device_model"),params.get("device_imei"),params.get("device_os_version"),params.get("device_id"),params.get("device_manufacture"));
        serverCall(call, responseListener);
    }

    public void Myacoounts(HashMap<String, String> params, ResponseListener responseListener) {

        Call<AccountModel> call = apiService.Myaccountnumbers(params.get("user_id"),
                params.get("mpin"),params.get("device_model"),params.get("device_imei"),params.get("device_os_version"),params.get("device_id"),params.get("device_manufacture"));
        serverCall(call, responseListener);
    }


    public void Mytransaction(HashMap<String, String> params, ResponseListener responseListener) {

        Call<TransactionModel> call = apiService.MyTransactiondetails(params.get("account_id"));
        serverCall(call, responseListener);
    }



//    public void getDiscountCoupons(String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<List<CouponResponseModel>> call = apiService.getDiscountCoupons(authToken);
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void deleteDiscountCoupon(CouponResponseModel couponResponseModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Log.d("DiscountId",new Gson().toJson(couponResponseModel.getId())+"");
//        Call<BaseResponseModel> call = apiService.deleteDiscountCoupon(authToken, new Gson().toJson(couponResponseModel.getId()));
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void getUsers(int currentPage, int pageSize,String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<TotalUserResponseModel> call = apiService.getUsers(authToken,currentPage,pageSize);
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void updateUser(UserRequestModel userRequestModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        Log.d("Request", new Gson().toJson(userRequestModel).toString());
//        if (dialog != null)
//            dialog.show();
//        Call<BaseResponseModel> call = apiService.updateUser(authToken, userRequestModel);
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void insertUser(UserRequestModel userRequestModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<BaseResponseModel> call = apiService.insertUser(authToken, userRequestModel);
//        serverCall(call, responseListener, dialog);
//    }
//    public void deleteUser(TotalUserResponseModel.User userResponseModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Log.d("USERID",new Gson().toJson(userResponseModel.getID()).toString());
//        Call<BaseResponseModel> call = apiService.deleteUser(authToken, new Gson().toJson(userResponseModel.getID()));
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void getProducts(int currentPage, int pageSize, String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<List<ProductResponseModel>> call = apiService.getProducts(authToken, currentPage, pageSize);
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void getProductSpinnerList(String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<ProductSpinnerResponseModel> call = apiService.getProductSpinnerList(authToken, "");
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void updateProduct(ProductResponseModel userRequestModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        Log.d("Request", new Gson().toJson(userRequestModel).toString());
//        if (dialog != null)
//            dialog.show();
//        Call<BaseResponseModel> call = apiService.updateProduct(authToken, userRequestModel);
//        serverCall(call, responseListener, dialog);
//    }
//
//    public void insertProduct(ProductResponseModel productResponseModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<BaseResponseModel> call = apiService.insertProduct(authToken, productResponseModel);
//        serverCall(call, responseListener, dialog);
//    }
//    public void deleteProduct(ProductResponseModel productResponseModel, String authToken, Dialog dialog, ResponseListener responseListener) {
//        if (dialog != null)
//            dialog.show();
//        Call<BaseResponseModel> call = apiService.deleteProduct(authToken,productResponseModel.getProductId()+"");
//        serverCall(call, responseListener, dialog);
//    }

    private <T> void serverCall(Call<T> call, final ResponseListener responseListener) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Log.d("Response",response.body().toString());
                responseListener.onResponseSuccess(response);
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                responseListener.onResponseFailure(t);
            }
        });
    }
}

