package com.example.hp.myapplication.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.hp.myapplication.MainActivity;
import com.example.hp.myapplication.R;


public class Splash extends RuntimePermissionsActivity
{
    private static int SPLASH_TIME_OUT = 3000;
    boolean cheeck;
     String id;
    private static final int REQUEST_PERMISSIONS = 20;
    private final int SPLASH_WAITING = 2000; //2 sec


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView txtmpassbok=(TextView)findViewById(R.id.txtmpassbok);
        TextView txtversion=(TextView)findViewById(R.id.txtversion);
        TextView txtcopyright=(TextView)findViewById(R.id.txtcopyright);

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtmpassbok.setTypeface(type);
        txtversion.setTypeface(type);
        txtcopyright.setTypeface(type);


        Splash.super.requestAppPermissions(new
                        String[]{
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.READ_PHONE_STATE,


                },R.string.runtime_permissions_txt
                , REQUEST_PERMISSIONS);

    }




    @Override
    public void onPermissionsGranted(int requestCode) {
        getSplashscreens();
    }


    public void getSplashscreens(){


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {



                Intent i = new Intent(Splash.this, MainActivity.class);
                startActivity(i);
                finish();


            }
        }, SPLASH_TIME_OUT);
    }

}
