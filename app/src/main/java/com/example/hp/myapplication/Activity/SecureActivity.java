package com.example.hp.myapplication.Activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.provider.Settings.Secure;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.myapplication.FireBase.Config;
import com.example.hp.myapplication.R;
import com.example.hp.myapplication.constants.url;
import com.example.hp.myapplication.models.BaseModel;
import com.example.hp.myapplication.models.OtpModel;
import com.example.hp.myapplication.models.VerifyotpModel;
import com.example.hp.myapplication.rest.NetworkController;
import com.example.hp.myapplication.rest.ResponseListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

import static java.security.AccessController.getContext;

public class SecureActivity extends AppCompatActivity {

        EditText editText1,editText2,editText3,editText4,editreText1,editreText2,editreText3,editreText4;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String userid;
    HashMap<String, String> params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure);

        BindId();
        Intent mintent=getIntent();
        userid=mintent.getStringExtra("userid");

        LinearLayout lnrdone=(LinearLayout)findViewById(R.id.lnrdone);
        lnrdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(editText1.getText().toString().trim().equals("")){
                    Toast.makeText(SecureActivity.this, "Please provide 4 digit pin", Toast.LENGTH_SHORT).show();
                }

                if(editText2.getText().toString().trim().equals("")){
                    Toast.makeText(SecureActivity.this, "Please provide 4 digit pin", Toast.LENGTH_SHORT).show();
                }

                if(editText3.getText().toString().trim().equals("")){
                    Toast.makeText(SecureActivity.this, "Please provide 4 digit pin", Toast.LENGTH_SHORT).show();
                }

                if(editText4.getText().toString().trim().equals("")){
                    Toast.makeText(SecureActivity.this, "Please provide 4 digit pin", Toast.LENGTH_SHORT).show();
                }

                if(!editText1.getText().toString().equals(editreText1.getText().toString())){
                    Toast.makeText(SecureActivity.this, "Pin not matched", Toast.LENGTH_SHORT).show();
                }
                else if(!editText2.getText().toString().equals(editreText2.getText().toString())){
                    Toast.makeText(SecureActivity.this, "Pin not matched", Toast.LENGTH_SHORT).show();
                }
                else if(!editText3.getText().toString().equals(editreText3.getText().toString())){
                    Toast.makeText(SecureActivity.this, "Pin not matched", Toast.LENGTH_SHORT).show();
                }

                else if(!editText4.getText().toString().equals(editreText4.getText().toString())){
                    Toast.makeText(SecureActivity.this, "Pin not matched", Toast.LENGTH_SHORT).show();
                }
                else {
                    String pin=editText1.getText().toString().trim()+editText2.getText().toString().trim()+editText3.getText().toString().trim()+editText4.getText().toString().trim();
                    setApppin(pin);
                }


            }
        });

        displayFirebaseRegId();


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText1.getText().toString().length()>0){
                    editText1.clearFocus();
                    editText2.requestFocus();
                    editText2.setCursorVisible(true);
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText2.getText().toString().length()>0){
                    editText2.clearFocus();
                    editText3.requestFocus();
                    editText3.setCursorVisible(true);
                }
                else {
                    editText2.clearFocus();
                    editText1.requestFocus();
                    editText1.setCursorVisible(true);
                }
            }
        });


        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText3.getText().toString().length()>0){
                    editText3.clearFocus();
                    editText4.requestFocus();
                    editText4.setCursorVisible(true);
                }
                else {
                    editText3.clearFocus();
                    editText2.requestFocus();
                    editText2.setCursorVisible(true);
                }
            }
        });

        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(editText4.getText().toString().length()>0){

                }
                else {
                    editText4.clearFocus();
                    editText3.requestFocus();
                    editText3.setCursorVisible(true);
                }


            }
        });

        //...reenter...///

        editreText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText1.getText().toString().length()>0){
                    editreText1.clearFocus();
                    editreText2.requestFocus();
                    editreText2.setCursorVisible(true);
                }
            }
        });


        editreText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText2.getText().toString().length()>0){
                    editreText2.clearFocus();
                    editreText3.requestFocus();
                    editreText3.setCursorVisible(true);
                }

                else {
                    editreText2.clearFocus();
                    editreText1.requestFocus();
                    editreText1.setCursorVisible(true);
                }
            }
        });


        editreText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText3.getText().toString().length()>0){
                    editreText3.clearFocus();
                    editreText4.requestFocus();
                    editreText4.setCursorVisible(true);
                }

                else {
                    editreText3.clearFocus();
                    editreText2.requestFocus();
                    editreText2.setCursorVisible(true);
                }
            }
        });
        editreText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText4.getText().toString().length()>0){

                }

                else {
                    editreText4.clearFocus();
                    editreText3.requestFocus();
                    editreText3.setCursorVisible(true);
                }


            }
        });

    }

    public void BindId(){
        editText1= (EditText) findViewById(R.id.editText1);
        editText2= (EditText) findViewById(R.id.editText2);
        editText3= (EditText) findViewById(R.id.editText3);
        editText4= (EditText) findViewById(R.id.editText4);
        editreText1= (EditText) findViewById(R.id.editreText1);
        editreText2= (EditText) findViewById(R.id.editreText2);
        editreText3= (EditText) findViewById(R.id.editreText3);
        editreText4= (EditText) findViewById(R.id.editreText4);



        TextView txtenroll=(TextView)findViewById(R.id.txtenroll);
        TextView txtverify=(TextView)findViewById(R.id.txtverify);
        TextView txtsecure=(TextView)findViewById(R.id.txtsecure);
        TextView txtenter4digit=(TextView)findViewById(R.id.txtenter4digit);
        TextView txtreenterpin=(TextView)findViewById(R.id.txtreenterpin);

        Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        txtenroll.setTypeface(type);
        txtverify.setTypeface(type);
        txtsecure.setTypeface(type);
        txtenter4digit.setTypeface(type1);
        txtreenterpin.setTypeface(type1);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //  txtMessage.setText(message);
                }
            }
        };

    }


    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        // Log.e(TAG, "Firebase reg id: " + regId);
        savefcm(regId);

        if (!TextUtils.isEmpty(regId)) {

        }
        else{

        }
        //  txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    public void savefcm(final String fcmnumber){
        final ProgressDialog pDailog = new ProgressDialog(SecureActivity.this);
        pDailog.setMessage("Loading");
        pDailog.setCancelable(false);
        pDailog.show();

        String tag_json_obj = "json_obj_req";
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                url.saveFcm,

                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDailog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.getString("code");

                            String msg=jsonObject.getString("message");

                            if (code.equals("1")) {

                            }else{

                            }


                        } catch (JSONException e) {
                            pDailog.dismiss();
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDailog.dismiss();

                Toast.makeText(SecureActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id",userid.trim());
                params.put("fcm",fcmnumber.trim());
                return params;




            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(SecureActivity.this);
        int socketTimeout = 15000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        requestQueue.add(jsonObjReq);


    }

    private void setApppin(final String pin) {

        String android_id = Secure.getString(SecureActivity.this.getContentResolver(),
                Secure.ANDROID_ID);

        String PhoneModel = android.os.Build.MODEL;


        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
       String imeinumber= telephonyManager.getDeviceId();

        String reqString = Build.MANUFACTURER;
        String androidversion=  Build.VERSION.RELEASE +" "+
              Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
        params=new HashMap<>();
        params.put("user_id",userid);
        params.put("mpin",pin);
        params.put("device_model",PhoneModel);
        params.put("device_imei",imeinumber);
        params.put("device_os_version",androidversion);
        params.put("device_id",android_id);
        params.put("device_manufacture",reqString);



        NetworkController.getInstance().Setapppin(params, new ResponseListener() {


            @Override
            public void onResponseSuccess(Response baseResponse) {
                String code= String.valueOf((baseResponse.code()));
                if  (code.equals("200")) {
                    BaseModel baseModel = (BaseModel) baseResponse.body();
                    processResponse(baseModel,pin);
                }

            }

            @Override
            public void onResponseFailure(Throwable throwable) {
                System.out.print("");
                Toast.makeText(SecureActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMessageRecieved(String msg) {

            }
        });
    }


    private void processResponse(BaseModel verifyotpModel,String pin) {
        String code = verifyotpModel.getCode();
        if (code.equals("1")) {
            Intent intent = new Intent(SecureActivity.this, HomeActivity.class);

            SharedPreferences.Editor editor = getSharedPreferences("BOIPASSBOOK", MODE_PRIVATE).edit();
            editor.putString("userid", userid);
            editor.putString("pin", pin);
            editor.apply();
            startActivity(intent);
            finish();
        }else{

            Toast.makeText(SecureActivity.this, "Wrong OTP", Toast.LENGTH_SHORT).show();
        }
    }

}
