/*
package com.example.hp.myapplication.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hp.myapplication.models.responeses.LoginResponseModel;
import com.example.hp.myapplication.rest.NetworkController;
import com.example.hp.myapplication.rest.ResponseListener;
import com.google.gson.Gson;

import retrofit2.Response;

import java.io.IOException;
import java.util.HashMap;

import static java.security.AccessController.getContext;

public class LoginFragment extends BlankFragment {

    private EditText etUserCode, etPassword;
    private Button btnLogin;

    private String userCode, password;
    View header;
    HashMap<String, String> params;

    public LoginFragment() {

    }

    @Override
    public void onMessageRecieved(String msg) {
        super.onMessageRecieved(msg);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initialise(rootView);
        return rootView;
    }

    private void initialise(View rootView) {
        etUserCode = (EditText) rootView.findViewById(R.id.etUserCode);
        etPassword = (EditText) rootView.findViewById(R.id.etPassword);
        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        header = getActivity().findViewById(R.id.header);
        header.setVisibility(View.GONE);


        setClicks();
    }

    private void setClicks() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (MethodConstant.isNetworkAvailble(getContext())) {
                        sendLoginRequest();
                    }else {
                        Toast.makeText(getContext(), "Please check network connection", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendLoginRequest() {
        NetworkController.getInstance().loginRequest(params, new LoadingDialog(getActivity()), new ResponseListener() {
            @Override
            public void onResponseSuccess(Response baseResponse, Dialog dialog) {
                if (baseResponse.code() == VariableConstant.RequestCode.SUCCESS) {
                    LoginResponseModel loginModel = (LoginResponseModel) baseResponse.body();
                    processResponse(loginModel);
                } else if (baseResponse.code() == VariableConstant.RequestCode.FAILURE) {
                    try {
                        ErrorResponseModel errorResponseModel = new Gson().fromJson(baseResponse.errorBody().string(), ErrorResponseModel.class);
                        Toast.makeText(getContext(), errorResponseModel.getError_description(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onResponseFailure(Throwable throwable, Dialog dialog) {
                System.out.print("");
                Toast.makeText(getContext(), throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void processResponse(LoginResponseModel loginModel) {
        PreferenceHelper.getInstance(getContext()).setStringValue(VariableConstant.AUTH_TOKEN, loginModel.getAccess_token());
        Intent intent = new Intent(getActivity(), MenuActivity.class);
        intent.putExtra("LoginResponseModel", loginModel);
        startActivity(intent);
    }

    private boolean validation() {
        userCode = etUserCode.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        if (userCode.length() == 0) {
            etUserCode.requestFocus();
            etUserCode.setError("Enter user code");
            return false;
        } else if (password.length() == 0) {
            etPassword.requestFocus();
            etPassword.setError("Enter password");
            return false;
        } else {
            params = new HashMap<>();
            params.put("userName", userCode);
            params.put("password", password);
            params.put("grantType", "password");
            return true;
        }
    }

}
*/
