package com.example.hp.myapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 22-08-2017.
 */

public class LoginModel {

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("mobile")
    @Expose
    private String mobile;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("message")
    @Expose

    private String message;



}
