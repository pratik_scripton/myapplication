    package com.example.hp.myapplication.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.myapplication.Adapters.MySpinnersAdapter;

import com.example.hp.myapplication.Adapters.ViewPagerAdapter;
import com.example.hp.myapplication.FireBase.Config;
import com.example.hp.myapplication.FireBase.NotificationUtils;
import com.example.hp.myapplication.Fragments.AllTransaction;
import com.example.hp.myapplication.Fragments.CreditTransaction;
import com.example.hp.myapplication.Fragments.DebitTransaction;
import com.example.hp.myapplication.Interfaces.DemoBase;
import com.example.hp.myapplication.Interfaces.PageRefreshListener;
import com.example.hp.myapplication.R;
import com.example.hp.myapplication.models.AccountModel;
import com.example.hp.myapplication.models.ListModel;
import com.example.hp.myapplication.models.TransactionModel;
import com.example.hp.myapplication.rest.NetworkController;
import com.example.hp.myapplication.rest.ResponseListener;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import retrofit2.Response;

    public class HomeActivity extends DemoBase implements OnChartValueSelectedListener  {
        private PieChart mChart;
        TabLayout tabLayout;
        ViewPager viewPager;
        LinearLayout lnr_userdetalis;
        Spinner spinner;
        AppBarLayout appbar;
        CollapsingToolbarLayout collapsing_toolbar;
        Toolbar toolbar;
        CoordinatorLayout main_content;
        private BroadcastReceiver mRegistrationBroadcastReceiver;
        private static final String TAG = HomeActivity.class.getSimpleName();
        ImageView imgprofile,imgnotification;
        SmoothProgressBar mProgressBar;
        float raintfall[]={50f,30f,77f};
        ArrayList<ListModel>accountnumber=new ArrayList<>();
        TextView txtmybalance;

        String monthofNames[]={"FD","RD","Loan"};
        HashMap<String, String> params;
        String userid;
        AllTransaction allTransaction;
        DebitTransaction debitTransaction;
        CreditTransaction creditTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        BindId();

        AccountDetails();

        onclick();

        setuppiechart();


    }




    public void BindId() {

            viewPager = (ViewPager) findViewById(R.id.viewpager);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
//        lnr_userdetalis = (LinearLayout) findViewById(R.id.lnr_userdetalis);
            collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        imgprofile= (ImageView) findViewById(R.id.imgprofile);
        imgnotification= (ImageView) findViewById(R.id.imgnotification);

            appbar = (AppBarLayout) findViewById(R.id.appbar);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
            main_content = (CoordinatorLayout) findViewById(R.id.main_content);



            TextView txtaccountnumber=(TextView)findViewById(R.id.txtaccountnumber);
            TextView txtbanlbalance=(TextView)findViewById(R.id.txtbanlbalance);
             txtmybalance=(TextView)findViewById(R.id.txtmybalance);
            TextView txtfd=(TextView)findViewById(R.id.txtfd);
            TextView txtrd=(TextView)findViewById(R.id.txtrd);
            TextView txtloan=(TextView)findViewById(R.id.txtloan);
            TextView txtfdamount=(TextView)findViewById(R.id.txtfdamount);
            TextView txtrdamount=(TextView)findViewById(R.id.txtrdamount);
            TextView txtloanamount=(TextView)findViewById(R.id.txtloanamount);

        allTransaction=new AllTransaction();
        debitTransaction=new DebitTransaction();
        creditTransaction=new CreditTransaction();

        mProgressBar = (SmoothProgressBar) findViewById(R.id.progressbar);


        mProgressBar.progressiveStart();

            Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                    "eras-demi-itc-5968a04fe8fff.ttf");

            Typeface type = Typeface.createFromAsset(this.getAssets(),
                    "DaxlinePro-Bold_13127.ttf");

            txtaccountnumber.setTypeface(type);
            txtbanlbalance.setTypeface(type);
            txtfd.setTypeface(type);
            txtrd.setTypeface(type);
            txtloan.setTypeface(type);
            txtmybalance.setTypeface(type1);
            txtfdamount.setTypeface(type1);
            txtrdamount.setTypeface(type1);
            txtloanamount.setTypeface(type1);
            changeTabsFont(String.valueOf(type));

            spinner = (Spinner) findViewById(R.id.spinner);



                  viewPager.setOffscreenPageLimit(2);
                tabLayout.setupWithViewPager(viewPager);


            mRegistrationBroadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    // checking for type intent filter
                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                        // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                        displayFirebaseRegId();

                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        // new push notification is received

                        String message = intent.getStringExtra("message");

                        Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                        //  txtMessage.setText(message);
                    }
                }
            };


        }

    public void onclick(){
        imgprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,ProfileActivity.class);
                startActivity(intent);
            }
        });

        imgnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,NotificationActivity.class);
                startActivity(intent);
            }
        });

        ImageView homeicon= (ImageView) findViewById(R.id.homeicon);
        homeicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBar.progressiveStop();
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String account=accountnumber.get(position).getAccount_id();

                Transactions(account);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void displayFirebaseRegId() {
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
            String regId = pref.getString("regId", null);

           // Log.e(TAG, "Firebase reg id: " + regId);

            if (!TextUtils.isEmpty(regId)) {
                String temp = ("Firebase Reg Id: " + regId);
            }
            else{

            }
              //  txtRegId.setText("Firebase Reg Id is not received yet!");
        }

    private void setuppiechart() {

            //populating a list of pieEntries
            List<PieEntry> pieEntries = new ArrayList<>();

            for (int i = 0; i < raintfall.length; i++) {

                pieEntries.add(new PieEntry(raintfall[i],monthofNames[i]));
            }

            PieDataSet dataset=new PieDataSet(pieEntries,"Rainfall");
            dataset.setColors(ColorTemplate.COLORFUL_COLORS);
            PieData data=new PieData(dataset);


            //Get the chart
            PieChart chart=(PieChart) findViewById(R.id.chart);
            chart.setData(data);
            chart.invalidate();
        chart.setDescription(null);

        }

        @Override
        protected void onResume() {
            super.onResume();

            // register GCM registration complete receiver
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.REGISTRATION_COMPLETE));

            // register new push message receiver
            // by doing this, the activity will be notified each time a new message arrives
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.PUSH_NOTIFICATION));

            // clear the notification area when the app is opened
            NotificationUtils.clearNotifications(getApplicationContext());
        }

        @Override
        public void onValueSelected(Entry e, Highlight h) {

        }

        @Override
        public void onNothingSelected() {

        }

        @Override
        protected void onPause() {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
            super.onPause();
        }


        private void setupViewPager(ViewPager viewPager, ArrayList<ListModel> transactionmodel) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            adapter.addFragment(allTransaction, "All");
            adapter.addFragment(debitTransaction, "Debit");
            adapter.addFragment(creditTransaction, "Credit");
            viewPager.setAdapter(adapter);
        }
        private void changeTabsFont(String type) {

            ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
            int tabsCount = vg.getChildCount();
            for (int j = 0; j < tabsCount; j++) {
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTypeface(Typeface.createFromFile(type));
                    }
                }
            }
        }

        private void AccountDetails() {

            String android_id = Settings.Secure.getString(HomeActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            String PhoneModel = android.os.Build.MODEL;


            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            String imeinumber= telephonyManager.getDeviceId();

            String reqString = Build.MANUFACTURER;
            String androidversion=  Build.VERSION.RELEASE +" "+
                    Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

            SharedPreferences prefs = getSharedPreferences("BOIPASSBOOK", MODE_PRIVATE);
            String userid = prefs.getString("userid", null);
            String pin = prefs.getString("pin", null);

            params=new HashMap<>();
            params.put("user_id",userid);
            params.put("mpin",pin);
            params.put("device_model",PhoneModel);
            params.put("device_imei",imeinumber);
            params.put("device_os_version",androidversion);
            params.put("device_id",android_id);
            params.put("device_manufacture",reqString);

            NetworkController.getInstance().Myacoounts(params, new ResponseListener() {


                @Override
                public void onResponseSuccess(Response baseResponse) {
                    String code= String.valueOf((baseResponse.code()));
                    if  (code.equals("200")) {
                        AccountModel accountModel = (AccountModel) baseResponse.body();
                        processResponse(accountModel);
                    }

                }

                @Override
                public void onResponseFailure(Throwable throwable) {
                    System.out.print("");
                    Toast.makeText(HomeActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onMessageRecieved(String msg) {
                }
            });
        }
        private void processResponse(AccountModel accountmodel) {
            String code = accountmodel.getCode();
            if (code.equals("1")) {


                accountnumber=accountmodel.getList();


                MySpinnersAdapter adapter = new MySpinnersAdapter(HomeActivity.this,accountnumber);
                // adapter.setDropDownViewResource(R.layout.addshopcat);
                spinner.setAdapter(adapter);
            }else{


            }
        }


        private void Transactions(final String account) {

            params=new HashMap<>();
            params.put("account_id",account);

            NetworkController.getInstance().Mytransaction(params, new ResponseListener() {


                @Override
                public void onResponseSuccess(Response baseResponse) {
                    String code= String.valueOf((baseResponse.code()));
                    if  (code.equals("200")) {
                        TransactionModel accountModel = (TransactionModel) baseResponse.body();
                        processResponse1(accountModel,account);
                    }

                }

                @Override
                public void onResponseFailure(Throwable throwable) {
                    System.out.print("");
                    Toast.makeText(HomeActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onMessageRecieved(String msg) {

                }
            });
        }

        private void processResponse1(TransactionModel transactionModel,String accountnumb) {
            String code = transactionModel.getCode();
            if (code.equals("1")) {

                txtmybalance.setText(transactionModel.getBalance());
                 ArrayList<ListModel>transaction = new ArrayList<>();
                transaction=transactionModel.getList();
                setupViewPager(viewPager,transaction);

                PageRefreshListener pageRefreshListener=allTransaction;
                pageRefreshListener.onPageRefresh(transaction);
                PageRefreshListener pageRefreshListener1=debitTransaction;
                pageRefreshListener1.onPageRefresh(transaction);
                PageRefreshListener pageRefreshListener2=creditTransaction;
                pageRefreshListener2.onPageRefresh(transaction);
            }else{


            }
        }


    }