package com.example.hp.myapplication.Activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.example.hp.myapplication.R;

public class SetNewApppinActivity extends AppCompatActivity {
    EditText editText1,editText2,editText3,editText4,editreText1,editreText2,editreText3,editreText4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_new_apppin);

        BindID();

        addtextchange();
    }

    public void BindID(){
        editText1= (EditText) findViewById(R.id.editText1);
        editText2= (EditText) findViewById(R.id.editText2);
        editText3= (EditText) findViewById(R.id.editText3);
        editText4= (EditText) findViewById(R.id.editText4);
        editreText1= (EditText) findViewById(R.id.editreText1);
        editreText2= (EditText) findViewById(R.id.editreText2);
        editreText3= (EditText) findViewById(R.id.editreText3);
        editreText4= (EditText) findViewById(R.id.editreText4);

        TextView textView5=(TextView)findViewById(R.id.textView5);
        TextView  txtretypepin=(TextView)findViewById(R.id.txtretypepin);
        TextView  textView=(TextView)findViewById(R.id.textView);

        Typeface type1 = Typeface.createFromAsset(this.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");

        Typeface type = Typeface.createFromAsset(this.getAssets(),
                "DaxlinePro-Bold_13127.ttf");

        textView5.setTypeface(type);
        txtretypepin.setTypeface(type);
        textView.setTypeface(type);
    }


    public void addtextchange(){
        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText1.getText().toString().length()>0){
                    editText1.clearFocus();
                    editText2.requestFocus();
                    editText2.setCursorVisible(true);
                }
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText2.getText().toString().length()>0){
                    editText2.clearFocus();
                    editText3.requestFocus();
                    editText3.setCursorVisible(true);
                }
                else {
                    editText2.clearFocus();
                    editText1.requestFocus();
                    editText1.setCursorVisible(true);
                }
            }
        });


        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editText3.getText().toString().length()>0){
                    editText3.clearFocus();
                    editText4.requestFocus();
                    editText4.setCursorVisible(true);
                }
                else {
                    editText3.clearFocus();
                    editText2.requestFocus();
                    editText2.setCursorVisible(true);
                }
            }
        });

        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(editText4.getText().toString().length()>0){

                }
                else {
                    editText4.clearFocus();
                    editText3.requestFocus();
                    editText3.setCursorVisible(true);
                }


            }
        });

        //...reenter...///

        editreText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText1.getText().toString().length()>0){
                    editreText1.clearFocus();
                    editreText2.requestFocus();
                    editreText2.setCursorVisible(true);
                }
            }
        });


        editreText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText2.getText().toString().length()>0){
                    editreText2.clearFocus();
                    editreText3.requestFocus();
                    editreText3.setCursorVisible(true);
                }

                else {
                    editreText2.clearFocus();
                    editreText1.requestFocus();
                    editreText1.setCursorVisible(true);
                }
            }
        });


        editreText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText3.getText().toString().length()>0){
                    editreText3.clearFocus();
                    editreText4.requestFocus();
                    editreText4.setCursorVisible(true);
                }

                else {
                    editreText3.clearFocus();
                    editreText2.requestFocus();
                    editreText2.setCursorVisible(true);
                }
            }
        });
        editreText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(editreText4.getText().toString().length()>0){

                }

                else {
                    editreText4.clearFocus();
                    editreText3.requestFocus();
                    editreText3.setCursorVisible(true);
                }


            }
        });

    }
}
