package com.example.hp.myapplication.Adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hp.myapplication.R;
import com.example.hp.myapplication.models.ListModel;

import java.util.ArrayList;

/**
 * Created by HP on 25-07-2017.
 */

public class MySpinnersAdapter extends BaseAdapter {


    ViewHolder holder;
    Context context;
    ArrayList<ListModel> accountnumbers;

    public MySpinnersAdapter(Context context, ArrayList<ListModel> accountnumbers) {
        super();
        this.accountnumbers = accountnumbers;
        this.context = context;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return accountnumbers.size();
    }

    @Override
    public Object getItem(int position) {

        return accountnumbers.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    static class ViewHolder {
        //  ImageView txtshopname;
        TextView txtaccountnumber;

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = new ViewHolder();
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.spinner_layout, null);
            //  holder.sub_cat_img=(ImageView) convertView.findViewById(R.id.sub_cat_img);

        }
        holder.txtaccountnumber=(TextView) convertView.findViewById(R.id.txtcatname);
        holder.txtaccountnumber.setText(accountnumbers.get(position).getAccount_id());

        Typeface type1 = Typeface.createFromAsset(context.getAssets(),
                "eras-demi-itc-5968a04fe8fff.ttf");
        holder.txtaccountnumber.setTypeface(type1);





        return convertView;

    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

    }

}


