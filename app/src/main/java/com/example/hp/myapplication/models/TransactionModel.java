package com.example.hp.myapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by HP on 21-08-2017.
 */

public class TransactionModel {

    @SerializedName("transaction_list")

    private ArrayList<ListModel> list = null;

    @SerializedName("code")
    String code;
    @SerializedName("balance")
    String balance;

    public ArrayList<ListModel> getList() {
        return list;
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }


}
