package com.example.hp.myapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 21-08-2017.
 */

public class ListModel {

    @SerializedName("account_id")
    @Expose
    private String account_id;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("transaction_narration")
    @Expose
    private String transaction_narration;

    @SerializedName("transaction_type")
    @Expose
    private String transaction_type;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransaction_narration() {
        return transaction_narration;
    }

    public void setTransaction_narration(String transaction_narration) {
        this.transaction_narration = transaction_narration;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }



    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }
}
