package com.example.hp.myapplication.models.responeses;

/**
 * Created by HP on 22-07-2017.
 */

public class BaseResponse {

    boolean isSucces;
    int id;

    public boolean isSucces() {
        return isSucces;
    }

    public void setSucces(boolean succes) {
        isSucces = succes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
