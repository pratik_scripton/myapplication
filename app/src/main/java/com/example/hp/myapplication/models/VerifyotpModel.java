package com.example.hp.myapplication.models;

import com.example.hp.myapplication.models.responeses.BaseResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 22-07-2017.
 */

public class VerifyotpModel {
    @SerializedName("code")
    String code;
    @SerializedName("user_id")
    String user_id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
