package com.example.hp.myapplication.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HP on 31-07-2017.
 */

public class OtpModel {
    @SerializedName("code")
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
