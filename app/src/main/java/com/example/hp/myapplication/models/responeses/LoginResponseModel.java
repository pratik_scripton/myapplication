package com.example.hp.myapplication.models.responeses;

import com.example.hp.myapplication.models.VerifyotpModel;

import java.util.ArrayList;

/**
 * Created by HP on 22-07-2017.
 */

public class LoginResponseModel extends BaseResponse{


    String userId,token;
    ArrayList<VerifyotpModel> fghfgh;

    public ArrayList<VerifyotpModel> getFghfgh() {
        return fghfgh;
    }

    public void setFghfgh(ArrayList<VerifyotpModel> fghfgh) {
        this.fghfgh = fghfgh;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
