package com.example.hp.myapplication.Interfaces;

import com.example.hp.myapplication.models.ListModel;

import java.util.ArrayList;

/**
 * Created by sachin on 27/8/17.
 */

public interface PageRefreshListener  {
    void onPageRefresh(ArrayList<ListModel> transactionmodel);
}
